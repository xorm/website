---
date: "2019-06-17T09:54:00+08:00"
title: "Core"
weight: 10
toc: false
draft: false
menu: "sidebar"
goimport: "xorm.io/core git https://gitea.com/xorm/core"
gosource: "xorm.io/core https://gitea.com/xorm/core https://gitea.com/xorm/core/tree/master{/dir} https://gitea.com/xorm/core/blob/master{/dir}/{file}#L{line}"
---

# Core - Lightweight & Compitable wrapper of database/sql

This is the URL of the import path for [Core](https://gitea.com/xorm/core).
