---
description: "XORM is a Simple & Powerful ORM Framework for Go Programming Language"
title: "XORM - eXtra ORM for Go"
draft: false
date: "2016-11-08T16:00:00+02:00"
weight: 10
toc: false
draft: false
url: "en"
type: "home"
---
<div id="main">
  <div id="hero">
    <h1>XORM - eXtra ORM for Go</h1>
    <p>XORM is a Simple & Powerful ORM Framework for Go Programming Language</p>
  </div>
  <div id="action-buttons">
    <a class="button primary big" href="https://gobook.io/read/gitea.com/xorm/manual-en-US/" onclick="_gaq.push(['_trackEvent', 'kube', 'download']);">Get Started</a> <a class="button outline big" href="https://gitea.com/xorm/xorm" onclick="_gaq.push(['_trackEvent', 'kube', 'github']);">View on Gitea.com</a>
    <p>Use <span style="background-color:#000000;color:#ffffff">go get xorm.io/xorm</span> download</p>
  </div>
  <div class="message focus" data-component="message"> <span class="close small"></span>
    <a class="button inverted small " href="#">Notice </a> XORM have been moved from <a href="https://github.com/go-xorm">Github</a> to <a href="https://gitea.com/xorm">Gitea</a> !
  </div>
  <div id="kube-features">
    <div class="row gutters">
      <div class="col col-4 item">
        <figure>
          <img alt="Baseline" height="48" src="/img/kube/icon-baseline.png" width="48">
        </figure>
        <h3>Easy Usage</h3>
        <p>By join function design, use less codes to finish DB operations.</p>
      </div>
      <div class="col col-4 item">
        <figure>
          <img alt="Typography" height="48" src="/img/kube/icon-typo.png" width="48">
        </figure>
        <h3>Rich Features</h3>
        <p>Support cache, transaction, Optimistic Lock, Multiple Database support, Reverse and etc.</p>
      </div>
      <div class="col col-4 item">
        <figure>
          <img alt="Minimalism" height="48" src="/img/kube/icon-minimalism.png" width="48">
        </figure>
        <h3>Open Source</h3>
        <p>Join, share and learn by involving this great project and be a contributor.</p>
      </div>
    </div>
    <div class="row gutters">
      <div class="col col-4 item">
        <h4>Installation</h4>
        <div class="markdown">
                <p>Install Library</p>
                <p><pre><code class="hljs">go get xorm.io/xorm</code></pre></p>
                <p>Reverse Tool</p>
                <p><pre><code class="hljs">go get xorm.io/reverse</code></pre></p>
                <p>Source Documents</p>
                <p><a href="https://pkg.go.dev/xorm.io/xorm" target="_blank">pkg.go.dev/xorm.io/xorm</a></p>
        </div>
      </div>
      <div class="col col-4 item">
        <h4>Run</h4>
        <div class="markdown">
                <p>Initilization</p>
                <p><pre><code class="hljs">orm, err := xorm.NewEngine("sqlite3", "./test.db")</code></pre></p>
                <p>Synchronize</p>
                <p><pre><code class="hljs">err = orm.Sync2(new(User), new(Article))</code></pre></p>
                <p>Now, you get it.</p>
        </div>
      </div>
      <div class="col col-4 item">
        <h4>Databases supported</h4>
        <div class="well markdown">
            <ul class="list-unstyled" style="list-style: none;">
              <li><i class="icon-angle-right"></i>Mysql: <a href="https://github.com/go-sql-driver/mysql">github.com/go-sql-driver/mysql</a></li>
              <li><i class="icon-angle-right"></i>MyMysql: <a href="https://github.com/ziutek/mymysql/godrv">github.com/ziutek/mymysql/godrv</a></li>
              <li><i class="icon-angle-right"></i>Postgres: <a href="https://github.com/lib/pq">github.com/lib/pq</a></li>
              <li><i class="icon-angle-right"></i>Tidb: <a href="https://github.com/pingcap/tidb">github.com/pingcap/tidb</a></li>
              <li><i class="icon-angle-right"></i>SQLite: <a href="https://github.com/mattn/go-sqlite3">github.com/mattn/go-sqlite3</a></li>
              <li><i class="icon-angle-right"></i>MsSql:<a href="https://github.com/denisenkom/go-mssqldb"> github.com/denisenkom/go-mssqldb</a></li>
              <li><i class="icon-angle-right"></i>MsSql: <a href="https://github.com/lunny/godbc">github.com/lunny/godbc</a></li>
              <li><i class="icon-angle-right"></i>Oracle: <a href="https://github.com/mattn/go-oci8">github.com/mattn/go-oci8</a></li>
            </ul>
        </div>
    </div>
  </div>
</div>
</div>

