---
date: "2019-10-14T17:26:00+08:00"
title: "XORM"
weight: 10
toc: false
draft: false
menu: "sidebar"
goimport: "xorm.io/xorm git https://gitea.com/xorm/xorm"
gosource: "xorm.io/xorm https://gitea.com/xorm/xorm https://gitea.com/xorm/xorm/src/branch/master{/dir} https://gitea.com/xorm/xorm/src/branch/master{/dir}/{file}#L{line}"
---

# XORM - Simple and Powerful ORM for Go

This is the URL of the import path for [XORM](https://gitea.com/xorm/xorm).
