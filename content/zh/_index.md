---
description: "XORM 是一个简单而强大的 Go 语言 ORM 框架"
title: "XORM - eXtra ORM for Go"
draft: false
date: "2016-11-08T16:00:00+02:00"
weight: 10
toc: false
draft: false
url: "zh"
type: "home"
---
<div id="main">
  <div id="hero">
    <h1>XORM - eXtra ORM for Go</h1>
    <p>XORM 是一个简单而强大的 Go 语言 ORM 框架</p>
  </div>
  <div id="action-buttons">
    <a class="button primary big" href="https://gobook.io/read/gitea.com/xorm/manual-zh-CN/" onclick="_gaq.push(['_trackEvent', 'kube', 'download']);">立即开始</a> <a class="button outline big" href="https://gitea.com/xorm/xorm" onclick="_gaq.push(['_trackEvent', 'kube', 'github']);">在 Gitea.com 查看</a>
    <p>使用 <span style="background-color:#000000;color:#ffffff">go get xorm.io/xorm</span> 下载</p>
  </div>
  <div class="message focus" data-component="message"> <span class="close small"></span>
    <a class="button inverted small " href="#">通知 </a> XORM 已经从 <a href="https://github.com/go-xorm">Github</a> 迁移到 <a href="https://gitea.com/xorm">Gitea</a> !
  </div>
  <div id="kube-features">
    <div class="row gutters">
      <div class="col col-4 item">
        <figure>
          <img alt="Baseline" height="48" src="/img/kube/icon-baseline.png" width="48">
        </figure>
        <h3>易使用</h3>
        <p>通过连写操作，可以通过很少的语句完成数据库操作。</p>
      </div>
      <div class="col col-4 item">
        <figure>
          <img alt="Typography" height="48" src="/img/kube/icon-typo.png" width="48">
        </figure>
        <h3>功能全</h3>
        <p>支持缓存，事务，乐观锁，多种数据库支持，反转等等特性。</p>
      </div>
      <div class="col col-4 item">
        <figure>
          <img alt="Minimalism" height="48" src="/img/kube/icon-minimalism.png" width="48">
        </figure>
        <h3>开源化</h3>
        <p>通过加入我们来参与、分享和学习，并成为一个贡献者。</p>
      </div>
    </div>
    <div class="row gutters">
      <div class="col col-4 item">
        <h4>安装</h4>
        <div class="markdown">
                <p>库安装</p>
                <p><pre><code class="hljs">go get xorm.io/xorm</code></pre></p>
                <p>Reverse工具</p>
                <p><pre><code class="hljs">go get xorm.io/reverse</code></pre></p>
                <p>源码文档</p>
                <p><a href="https://pkg.go.dev/xorm.io/xorm" target="_blank">pkg.go.dev/xorm.io/xorm</a></p>
        </div>
      </div>
      <div class="col col-4 item">
        <h4>运行</h4>
        <div class="markdown">
                <p>初始化</p>
                <p><pre><code class="hljs">orm, err := xorm.NewEngine("sqlite3", "./test.db")</code></pre></p>
                <p>同步数据库结构</p>
                <p><pre><code class="hljs">err = orm.Sync2(new(User), new(Article))</code></pre></p>
                <p>恭喜！您已经成功地运行了 XORM。</p>
        </div>
      </div>
      <div class="col col-4 item">
        <h4>数据库支持</h4>
        <div class="well markdown">
            <ul class="list-unstyled" style="list-style: none;">
              <li><i class="icon-angle-right"></i>Mysql: <a href="https://github.com/go-sql-driver/mysql">github.com/go-sql-driver/mysql</a></li>
              <li><i class="icon-angle-right"></i>MyMysql: <a href="https://github.com/ziutek/mymysql/godrv">github.com/ziutek/mymysql/godrv</a></li>
              <li><i class="icon-angle-right"></i>Postgres: <a href="https://github.com/lib/pq">github.com/lib/pq</a></li>
              <li><i class="icon-angle-right"></i>Tidb: <a href="https://github.com/pingcap/tidb">github.com/pingcap/tidb</a></li>
              <li><i class="icon-angle-right"></i>SQLite: <a href="https://github.com/mattn/go-sqlite3">github.com/mattn/go-sqlite3</a></li>
              <li><i class="icon-angle-right"></i>MsSql:<a href="https://github.com/denisenkom/go-mssqldb"> github.com/denisenkom/go-mssqldb</a></li>
              <li><i class="icon-angle-right"></i>MsSql: <a href="https://github.com/lunny/godbc">github.com/lunny/godbc</a></li>
              <li><i class="icon-angle-right"></i>Oracle: <a href="https://github.com/mattn/go-oci8">github.com/mattn/go-oci8</a></li>
            </ul>
        </div>
    </div>
  </div>
</div>
</div>